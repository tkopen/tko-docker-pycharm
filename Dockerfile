# Build
# FROM_IMG=ubuntu:22.04; CI_REGISTRY_IMAGE=tko-docker-pycharm; PYCHARM_VERSION=2022.3.3; docker build --pull --build-arg FROM_IMG=$FROM_IMG --build-arg PYCHARM_VERSION=$PYCHARM_VERSION -t $CI_REGISTRY_IMAGE:cpu .
# FROM_IMG=tensorflow/tensorflow:2.12.0-gpu-jupyter; CI_REGISTRY_IMAGE=tko-docker-pycharm; PYCHARM_VERSION=2022.3.3; docker build --pull --build-arg FROM_IMG=$FROM_IMG --build-arg PYCHARM_VERSION=$PYCHARM_VERSION -t $CI_REGISTRY_IMAGE:gpu-tf-jupyter .
#
# Run
# docker run -it --rm --name pycharm --net host -v /run/user/$UID/keyring/ssh:/ssh-agent -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY tko-docker-pycharm:cpu pycharm
# docker run -it --gpus all --rm --name pycharm --net host -v /run/user/$UID/keyring/ssh:/ssh-agent -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=unix$DISPLAY -p 8888:8888 tko-docker-pycharm:gpu-tf-jupyter

ARG FROM_IMG=ubuntu:22.04
FROM $FROM_IMG

LABEL maintainer="TKOpen create issues sending email to: incoming+tkopen-tko-docker-pycharm-44674135-1dh8c3icqcqf9clbbd1ade7c8-issue@incoming.gitlab.com"
LABEL version="1.0"

SHELL ["/bin/bash", "-c"]

ARG PYCHARM_VERSION=2022.3.3

ENV DEBIAN_FRONTEND=noninteractive \
    PYCHARM_VERSION=${PYCHARM_VERSION} \
    TERM=xterm \
    USER_NAME=coder

### APT INSTALLATION
RUN apt update && apt -y dist-upgrade \
    && apt -y autoremove \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    && sync
RUN apt update && apt install -y --no-install-recommends \
        # Standard
        bash-completion \
        curl \
        g++ \
        gcc \
        git \
        locales \
        python3-dev \
        python3-pip \
        sudo \
        ssh \
        # For Pycharm
        libfreetype6 \
        libxext6 \
        libxi6 \
        libxrender1 \
        libxtst6 \
        openjdk-11-jre \
    && useradd \
        --create-home \
        --home /home/${USER_NAME} \
        --skel /etc/skel \
        --shell /bin/bash \
        --groups sudo \
        ${USER_NAME} \
    && sed -i "s/%sudo.*ALL=(ALL:ALL) ALL/%sudo ALL=(ALL:ALL) NOPASSWD:ALL/" /etc/sudoers \
    && apt -y autoremove \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    && sync

### LANGUAGE
ENV LANG=en_US.UTF-8 \
    LANGUAGE=$LANG
RUN locale-gen --no-purge en_US en_US.UTF-8 pt_PT pt_PT.UTF-8 pt_BR pt_BR.UTF-8 $LANG \
    && update-locale LANG=$LANG \
    && echo locales locales/locales_to_be_generated multiselect $LANG UTF-8 | debconf-set-selections \
    && echo locales locales/default_environment_locale select $LANG | debconf-set-selections \
    && export LANG=$LANG \
    && export LANGUAGE=$LANG \
    && sync

# INSTALL PYCHARM
RUN echo "Downloading and installing pycharm-community-${PYCHARM_VERSION}..." \
    && curl -s -L -o - https://download.jetbrains.com/python/pycharm-community-${PYCHARM_VERSION}.tar.gz | tar xz -C /opt \
    && ln -s /opt/pycharm-community-${PYCHARM_VERSION}/bin/pycharm.sh /usr/local/bin/pycharm \
    && echo "PyCharm installed" \
    && apt -y autoremove \
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    && sync
    
USER ${USER_NAME}
WORKDIR /home/${USER_NAME}/workspace

# PyCharm: Creating the directories to be used by PyCharm configurations, to be mounted as volumes with correct user and group
RUN mkdir -p /home/${USER_NAME}/.cache/JetBrains \
    && mkdir -p /home/${USER_NAME}/.config/JetBrains \
    && mkdir -p /home/${USER_NAME}/.java \
    && mkdir -p /home/${USER_NAME}/.local/share/JetBrains \
    && mkdir -p /home/${USER_NAME}/workspace/.idea

